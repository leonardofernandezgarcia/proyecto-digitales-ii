/*
 * @brief Blinky example using sysTick
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2014
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "chip.h"
#include "board.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define TICKRATE_HZ1 (10)	/* 10 ticks per second */
#define CANTIDAD_DE_MUESTRAS 4096
#define DOWNSAMPLE_RATE 1
#define DECIMALES 2
#define DECIMAL_SEPARATOR "."
#define	SAMPLE_RATE 44100 // Sample Rate = 44.1kHz

// Error admitido para determinar que es una nota
#define	ERROR_RELATIVO_ADMISIBLE 0.013

/* Puertos
 * GPIO 0.9  - P0.9  - Pin 5  - LEFT_DOWN
 * GPIO 0.8  - P0.8  - Pin 6  - DOWN
 * GPIO 0.7  - P0.7  - Pin 7  - CENTER
 * GPIO 0.6  - P0.6  - Pin 8  - LEFT_UP
 * GPIO 0.0  - P0.0  - Pin 9  - UP
 * GPIO 0.1  - P0.1  - Pin 10 - RIGHT_UP
 * GPIO 0.18 - P0.18 - Pin 11 - RIGHT_DOWN
 * GPIO 0.17 - P0.17 - Pin 12 - DOT
 * GPIO 0.15 - P0.15 - Pin 13 - LED_DOWN
 * GPIO 0.16 - P0.16 - Pin 14 - LED_OK
 * GPIO 0.2  - P0.2  - Pin 21 - LED_UP
 * ADC 0.0   - P0.13 - Pin 15 - ADC0
 */

// Definicion de los segmentos del 7seg
#define LEFT_DOWN 9
#define DOWN 8
#define CENTER 7
#define LEFT_UP 6
#define UP 0
#define RIGHT_UP 1
#define RIGHT_DOWN 18
#define DOT 17
#define LED_DOWN 15
#define LED_OK 16
#define LED_UP 2

// Defino si pull down o pull up
#define	PRENDER	true	// Prendo con 1
#define APAGAR false	// Apago con 0

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
/*
 *  Tabla de notas
 *  Cada nota es un array de floats
 *  El array contiene la frecuencia de Hz de esa nota en las octavas 1 a la 7 respectivamente
 */
float A[7] = {55.00, 110.00, 220.00, 440.00, 880.00, 1760.00, 3520.00};
float ADot[7] = {58.27, 116.54, 233.08, 466.00, 932.33, 1864.66, 3729.31};
float B[7] = {61.74, 123.47, 246.94, 493.88, 987.77, 1975.53, 3951.07};
float C[7] = {32.70, 65.41, 130.81, 261.63, 523.25, 1046.50, 2093.00};
float CDot[7] = {34.65, 69.30, 138.59, 277.18, 554.37, 1108.73, 2217.46};
float D[7] = {36.71, 73.42, 146.83, 293.66, 587.33, 1174.66, 2349.32};
float DDot[7] = {38.89, 77.78, 155.56, 311.13, 622.25, 1244.51, 2489.02};
float E[7] = {41.20, 82.41, 164.81, 329.63, 659.26, 1318.51, 2637.02};
float F[7] = {43.65, 87.31, 174.61, 349.23, 698.46, 1396.91, 2793.83};
float FDot[7] = {46.25, 92.50, 185.00, 369.99, 739.99, 1479.98, 2959.96};
float G[7] = {49.00, 98.00, 196.00, 392.00, 783.99, 1567.98, 3135.96};
float GDot[7] = {51.91, 103.83, 207.65, 415.3, 830.61, 1661.22, 3322.44};

/*
 * Variables globales
 */
uint16_t datosADC0[CANTIDAD_DE_MUESTRAS];
uint16_t datosADC1[CANTIDAD_DE_MUESTRAS];
short int datosADCDownsampled[CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE];
int nro_muestra = 0;
static ADC_CLOCK_SETUP_T ADCSetup;

/*****************************************************************************
 * Private functions
 ****************************************************************************/


/*****************************************************************************
 * Public functions
 ****************************************************************************/
void prvSetupHardware(void);
void EscribirA(void);
void EscribirADot(void);
void EscribirB(void);
void EscribirC(void);
void EscribirCDot(void);
void EscribirD(void);
void EscribirDDot(void);
void EscribirE(void);
void EscribirF(void);
void EscribirFDot(void);
void EscribirG(void);
void EscribirGDot(void);
void EscribirLedDown(void);
void EscribirLedUp(void);
void EscribirLedOk(void);

void DownsampleADC0(void);
void DownsampleADC1(void);
int CountPeriods(void);
int CountSamplesInPeriod(void);
void processsFrequency(float frequency);
float calcFrequencyWithSamples(int samples);
float calcFrequencyWithPeriods(int periods);
/*
 * Handler de la interrupcion del ADC
 * Implementa un Pipelining entre los buffer datosADC0 y datosADC1
 */
void ADC_IRQHandler() {
	uint16_t dataADC;

	Chip_ADC_ReadValue(LPC_ADC, ADC_CH0, &dataADC);

	if (nro_muestra == 3*CANTIDAD_DE_MUESTRAS) {
		nro_muestra = nro_muestra - 2*CANTIDAD_DE_MUESTRAS;
	}

	if (nro_muestra < CANTIDAD_DE_MUESTRAS ) {
		datosADC0[nro_muestra] = dataADC;
	} else if (nro_muestra <= 2*CANTIDAD_DE_MUESTRAS) {
		datosADC1[(nro_muestra - CANTIDAD_DE_MUESTRAS)] = dataADC;
	} else if ( nro_muestra >= 2*CANTIDAD_DE_MUESTRAS ) {
		datosADC0[(nro_muestra - 2*CANTIDAD_DE_MUESTRAS)] = dataADC;
	}

	nro_muestra++;
}

/****************************************
 **************** Main ******************
 ****************************************
 */
int main(void)
{
	/* Initialization */
	prvSetupHardware();


	/* LEDs toggle in interrupt handlers */
	while (1) {

		//nro_actual = nro_muestra;
		//int crucesPorCero;
		float frecuencia;
		int cantidadDeMuestrasEnPeriodo;
		int CantidadDePeriodos;
		if (nro_muestra >= CANTIDAD_DE_MUESTRAS && nro_muestra < 2*CANTIDAD_DE_MUESTRAS) {
			/*
			 *  Hago Downsample de la señal muestreada
			 */
			DownsampleADC0();
			/*
			 * Cuento la cantidad de muestras de un periodo
			 */
			cantidadDeMuestrasEnPeriodo = CountSamplesInPeriod();
			/*
			 * Si es poca la cantidad de muestras,
			 * cuento la cantidad periodos dentro del muestreo
			 */
			if (cantidadDeMuestrasEnPeriodo <= 80) {
					CantidadDePeriodos = CountPeriods();
					/*
					 * Calculo la frecuencia con los periodos
					 */
					frecuencia = calcFrequencyWithPeriods(CantidadDePeriodos);
			} else {
				/*
				 *  Calculo frecuencia con los samples en un periodo
				 */
				frecuencia = calcFrequencyWithSamples(cantidadDeMuestrasEnPeriodo);
			}
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, UP, PRENDER);
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, RIGHT_UP, APAGAR);
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, LEFT_UP, APAGAR);
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, CENTER, APAGAR);
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, RIGHT_DOWN, APAGAR);
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, LEFT_DOWN, APAGAR);
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, DOWN, APAGAR);
			Chip_GPIO_WritePortBit(LPC_GPIO, 0, DOT, APAGAR);
			/*
			 * Proceso esa frecuencia para determinar:
			 * 1. Que octava es
			 * 2. Que nota es dentro de la octava
			 * 3. Si esta afinada o no
			 */
			processsFrequency(frecuencia);
		} else if (nro_muestra >= 2*CANTIDAD_DE_MUESTRAS) {
			/*
			 *  Hago Downsample de la señal muestreada
			 */
			DownsampleADC1();
			/*
			 * Cuento la cantidad de muestras de un periodo
			 */
			cantidadDeMuestrasEnPeriodo = CountSamplesInPeriod();
			/*
			 * Si es poca la cantidad de muestras,
			 * cuento la cantidad periodos dentro del muestreo
			 */
			if (cantidadDeMuestrasEnPeriodo <= 80) {
					CantidadDePeriodos = CountPeriods();
					/*
					 * Calculo la frecuencia con los periodos
					 */
					frecuencia = calcFrequencyWithPeriods(CantidadDePeriodos);
			} else {
				/*
				 *  Calculo frecuencia con los samples en un periodo
				 */
				frecuencia = calcFrequencyWithSamples(cantidadDeMuestrasEnPeriodo);
			}

			/*
			 * Proceso esa frecuencia para determinar:
			 * 1. Que octava es
			 * 2. Que nota es dentro de la octava
			 * 3. Si esta afinada o no
			 */
			processsFrequency(frecuencia);
		}
	}
	return 0;
}

/*
 * Funcion que inicializa el Hardware
 * GPIO y ADC
 */
void prvSetupHardware(void) {
	SystemCoreClockUpdate();
	Board_Init();
	//ADC p0.23 conector 15
	Chip_ADC_Init(LPC_ADC, &ADCSetup); // Inicializo el modulo de ADC
	Chip_ADC_SetSampleRate(LPC_ADC, &ADCSetup, SAMPLE_RATE); // Seteo Sample rate
	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH0, ENABLE); // Se habilita el canal 0: pin 15 p0.23
	Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE); // Seteo el ADC en BURST MODE
	Chip_IOCON_PinMux(LPC_IOCON,0,23,IOCON_MODE_INACT,IOCON_FUNC1); // Seteo el p0.23 como ADC
	Chip_IOCON_PinMux(LPC_IOCON,0,UP,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,RIGHT_UP,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,LEFT_UP,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,CENTER,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,LEFT_DOWN,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,RIGHT_DOWN,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,DOWN,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,DOT,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,LED_UP,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,LED_DOWN,IOCON_MODE_INACT,IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON,0,LED_OK,IOCON_MODE_INACT,IOCON_FUNC0);
	NVIC_EnableIRQ(ADC_IRQn); // Habilito interrupcion ADC
	Chip_ADC_Int_SetChannelCmd(LPC_ADC, ADC_CH0, ENABLE); // Habilito la interrupción de ADC 0
	Chip_ADC_SetStartMode(LPC_ADC,ADC_START_NOW,ADC_TRIGGERMODE_RISING);
	Chip_GPIO_Init(LPC_GPIO); // Inicializa el modulo de GPIO
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, UP);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, RIGHT_UP);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, LEFT_UP);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, CENTER);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, LEFT_DOWN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, RIGHT_DOWN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, DOWN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, DOT);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, LED_UP);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, LED_DOWN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, LED_OK);
}

/*
 * Funcion que hace DownSample del Buffer datosADC0
 */
void DownsampleADC0(void) {
	for(int a=0; a<(CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE); a++) {
		datosADCDownsampled[a] = 0;
		for (int b=0; b<DOWNSAMPLE_RATE; b++) {
			datosADCDownsampled[a] += datosADC0[DOWNSAMPLE_RATE*a + 2*b];
		}
		datosADCDownsampled[a] = ((datosADCDownsampled[a] / DOWNSAMPLE_RATE) - 2048 );
	}
}

/*
 * Funcion que hace DownSample del Buffer datosADC1
 */
void DownsampleADC1(void) {
	for(int a=0; a<(CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE); a++) {
		datosADCDownsampled[a] = 0;
		for (int b=0; b<DOWNSAMPLE_RATE; b++) {
			datosADCDownsampled[a] += datosADC1[DOWNSAMPLE_RATE*a + 2*b];
		}
		datosADCDownsampled[a] = ((datosADCDownsampled[a] / DOWNSAMPLE_RATE) - 2048 );
	}
}


/*
 * Funcion que cuenta la cantidad de cruces por 0 de una senal
 * Devuelve la cantidad de cruces en forma de int
 */
int CountPeriods(void) {
	int cruces = 0;
	int periods = 0;
	for(int a=1; a<(CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE); a++) {
		if ((datosADCDownsampled[a-1] >= 0) && (datosADCDownsampled[a] < 0) && !(datosADCDownsampled[a-1]==datosADCDownsampled[a])){
			cruces++;
			if ((cruces % 2) == 0) {
				periods++;
			}
		} else if ((datosADCDownsampled[a-1] <= 0) && (datosADCDownsampled[a] > 0) && !(datosADCDownsampled[a-1]==datosADCDownsampled[a])) {
			cruces++;
			if ((cruces % 2) == 0) {
				periods++;
			}
		}
	}
	return periods;
}

/*
 * Funcion que cuenta la cantidad de Samples
 * que hay dentro de un periodo de la senal
 * Devuelve el valor en un int
 */
int CountSamplesInPeriod(void) {
	int cruces = 0;
	int last_cross_zero_index = 0;
	float samples = 0;
	int count = 0;
	for(int a=1; a<(CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE); a++) {
		if ((datosADCDownsampled[a-1] > 0) && (datosADCDownsampled[a] < 0) && !(datosADCDownsampled[a-1]==datosADCDownsampled[a])){
			// Cuento los cruces por 0
			cruces++;
			// En cada senal completa sumo la cantidad de muestras
			// de la senal en ese periodo. ((cruces % 2) == 0)
			// Desecho el primer periodo (cruces > 2).
			// Desecho el ultimo periodo (no se incluye porque no da un cruce multiplo de 2)
			if (((cruces % 2) == 0) && (cruces > 2)) {
				samples = samples + a - last_cross_zero_index;
				last_cross_zero_index = a;
				count++;
			}
		} else if ((datosADCDownsampled[a-1] < 0) && (datosADCDownsampled[a] > 0) && !(datosADCDownsampled[a-1]==datosADCDownsampled[a])) {
			// Cuento los cruces por 0
			cruces++;
			// En cada senal completa sumo la cantidad de muestras
			// de la senal en ese periodo. ((cruces % 2) == 0)
			// Desecho el primer periodo (cruces > 2).
			// Desecho el ultimo periodo (no se incluye porque no da un cruce multiplo de 2)
			if (((cruces % 2) == 0) && (cruces > 2)) {
				samples = samples + a - last_cross_zero_index;
				last_cross_zero_index = a;
				count++;
			}
		}
	}
	// Divido la sumatoria de muestras sobre la cantidad de veces que le sume.
	int samples_int = samples/count;
	return samples_int;
}

/*
 * Funcion que procesa una frecuencia para determinar:
 * 1. Que octava es
 * 2. Que nota es dentro de la octava
 * 3. Si esta afinada o no
 */
void processsFrequency(float frequency) {
	/*
	 * Step 1 - Elegir octava
	 */
	int octava;
	// Octava 1
	if ( (frequency < ((B[0]+C[1])/2)) ) {
		octava = 0;
	}
	// Octava 2
	else if ( (frequency > ((B[0]+C[1])/2)) && (frequency < ((B[1]+C[2])/2)) ) {
		octava = 1;
	}
	// Octava 3
	else if ( (frequency > ((B[1]+C[2])/2)) && (frequency < ((B[2]+C[3])/2)) ) {
		octava = 2;
	}
	// Octava 4
	else if ( (frequency > ((B[2]+C[3])/2)) && (frequency < ((B[3]+C[4])/2)) ) {
		octava = 3;
	}
	// Octava 5
	else if ( (frequency > ((B[3]+C[4])/2)) && (frequency < ((B[4]+C[5])/2)) ) {
		octava = 4;
	}
	// Octava 6
	else if ( (frequency > ((B[4]+C[5])/2)) && (frequency < ((B[5]+C[6])/2)) ) {
		octava = 5;
	}
	// Octava 7
	else if ( (frequency > ((B[5]+C[6])/2)) ) {
		octava = 6;
	}
	/*
	 * Step 2 Teniendo octava calculo que nota.
	 */
	char nota;
	int dot;
	// C
	if ( (frequency < ((C[octava]+CDot[octava])/2)) ) {
		nota = 'C';
		dot = 0;
		EscribirC();
	}
	// CDot
	else if ( (frequency > ((C[octava]+CDot[octava])/2)) && (frequency < ((CDot[octava]+D[octava])/2)) ) {
		EscribirCDot();
		nota = 'C';
		dot = 1;
	}
	// D
	else if ( (frequency > ((CDot[octava]+D[octava])/2)) && (frequency < ((D[octava]+DDot[octava])/2)) ) {
		EscribirD();
		nota = 'D';
		dot = 0;
	}
	// DDot
	else if ( (frequency > ((D[octava]+DDot[octava])/2)) && (frequency < ((DDot[octava]+E[octava])/2)) ) {
		EscribirDDot();
		nota = 'D';
		dot = 1;
	}
	// E
	else if ( (frequency > ((DDot[octava]+E[octava])/2)) && (frequency < ((E[octava]+F[octava])/2)) ) {
		EscribirE();
		nota = 'E';
		dot = 0;
	}
	// F
	else if ( (frequency > ((E[octava]+F[octava])/2)) && (frequency < ((F[octava]+FDot[octava])/2)) ) {
		EscribirF();
		nota = 'F';
		dot = 0;
	}
	// FDot
	else if ( (frequency > ((F[octava]+FDot[octava])/2)) && (frequency < ((FDot[octava]+G[octava])/2)) ) {
		EscribirFDot();
		nota = 'F';
		dot = 1;
	}
	// G
	else if ( (frequency > ((FDot[octava]+G[octava])/2)) && (frequency < ((G[octava]+GDot[octava])/2)) ) {
		EscribirG();
		nota = 'G';
		dot = 0;
	}
	// GDot
	else if ( (frequency > ((G[octava]+GDot[octava])/2)) && (frequency < ((GDot[octava]+A[octava])/2)) ) {
		EscribirGDot();
		nota = 'G';
		dot = 1;
	}
	// A
	else if ( (frequency > ((GDot[octava]+A[octava])/2)) && (frequency < ((A[octava]+ADot[octava])/2)) ) {
		EscribirA();
		nota = 'A';
		dot = 0;
	}
	// ADot
	else if ( (frequency > ((A[octava]+ADot[octava])/2)) && (frequency < ((ADot[octava]+B[octava])/2)) ) {
		EscribirADot();
		nota = 'A';
		dot = 1;
	}
	// B
	else if ( (frequency > ((ADot[octava]+B[octava])/2))) {
		EscribirB();
		nota = 'B';
		dot = 0;
	}
	/*
	 * Step 3 - Ver si esta afinado
	 */
	switch(nota) {
		case 'C':
			if (dot == 0) {
				if (frequency < (C[octava]-(ERROR_RELATIVO_ADMISIBLE*C[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (C[octava]+(ERROR_RELATIVO_ADMISIBLE*C[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			} else {
				if (frequency < (CDot[octava]-(ERROR_RELATIVO_ADMISIBLE*CDot[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (CDot[octava]+(ERROR_RELATIVO_ADMISIBLE*CDot[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			}
			break;
		case 'D':
			if (dot == 0) {
				if (frequency < (D[octava]-(ERROR_RELATIVO_ADMISIBLE*D[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (D[octava]+(ERROR_RELATIVO_ADMISIBLE*D[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			} else {
				if (frequency < (DDot[octava]-(ERROR_RELATIVO_ADMISIBLE*DDot[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (DDot[octava]+(ERROR_RELATIVO_ADMISIBLE*DDot[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			}
			break;
		case 'E':
			if (frequency < (E[octava]-(ERROR_RELATIVO_ADMISIBLE*E[octava])) ) {
				EscribirLedDown();
			} else if (frequency > (E[octava]+(ERROR_RELATIVO_ADMISIBLE*E[octava])) ) {
				EscribirLedUp();
			} else {
				EscribirLedOk();
			}
			break;
		case 'F':
			if (dot == 0) {
				if (frequency < (F[octava]-(ERROR_RELATIVO_ADMISIBLE*F[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (F[octava]+(ERROR_RELATIVO_ADMISIBLE*F[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			} else {
				if (frequency < (FDot[octava]-(ERROR_RELATIVO_ADMISIBLE*FDot[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (FDot[octava]+(ERROR_RELATIVO_ADMISIBLE*FDot[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			}
			break;
		case 'G':
			if (dot == 0) {
				if (frequency < (G[octava]-(ERROR_RELATIVO_ADMISIBLE*G[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (G[octava]+(ERROR_RELATIVO_ADMISIBLE*G[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			} else {
				if (frequency < (GDot[octava]-(ERROR_RELATIVO_ADMISIBLE*GDot[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (GDot[octava]+(ERROR_RELATIVO_ADMISIBLE*GDot[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			}
			break;
		case 'A':
			if (dot == 0) {
				if (frequency < (A[octava]-(ERROR_RELATIVO_ADMISIBLE*A[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (A[octava]+(ERROR_RELATIVO_ADMISIBLE*A[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			} else {
				if (frequency < (ADot[octava]-(ERROR_RELATIVO_ADMISIBLE*ADot[octava])) ) {
					EscribirLedDown();
				} else if (frequency > (ADot[octava]+(ERROR_RELATIVO_ADMISIBLE*ADot[octava])) ) {
					EscribirLedUp();
				} else {
					EscribirLedOk();
				}
			}
			break;
		case 'B':
			if (frequency < (B[octava]-(ERROR_RELATIVO_ADMISIBLE*B[octava])) ) {
				EscribirLedDown();
			} else if (frequency > (B[octava]+(ERROR_RELATIVO_ADMISIBLE*B[octava])) ) {
				EscribirLedUp();
			} else {
				EscribirLedOk();
			}
			break;
	}
}

/*
 * Funcion que calcula la frecuencia
 * Tiene en cuenta:
 * 1. El SampleRate definido
 * 2. El DownsampleRate definido
 * 3. La cantidad de muestras de un período
 *
 * Devuelve la frecuencia en Hertz en un float
 */
float calcFrequencyWithSamples(int samples) {
	int pre = samples * DOWNSAMPLE_RATE;
	float period = (float)((float)pre / (float)(SAMPLE_RATE+(0.1*SAMPLE_RATE)));
	return (1.0f/period);
}

float calcFrequencyWithPeriods(int periods) {
	float periodSample = (float)((float)CANTIDAD_DE_MUESTRAS/(float)(SAMPLE_RATE+(0.06*SAMPLE_RATE)));
	float period = (float)(periodSample / (float) periods);
	return (1.0f/period);
}


/*
 * Funciones de escritura de Letras en Display de 7 Segmentos
 */
void EscribirA(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, APAGAR);
}

void EscribirADot(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, PRENDER);
}

void EscribirB(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, APAGAR);
}

void EscribirC(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, APAGAR);
}

void EscribirCDot(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, PRENDER);
}

void EscribirD(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, APAGAR);
}

void EscribirDDot(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, PRENDER);
}

void EscribirE(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, APAGAR);
}

void EscribirF(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, APAGAR);
}

void EscribirFDot(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, PRENDER);
}

void EscribirG(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, APAGAR);
}

void EscribirGDot(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_UP, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_UP, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, CENTER, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, RIGHT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LEFT_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, DOT, PRENDER);
}

void EscribirLedDown(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_DOWN, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_OK, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_UP, APAGAR);
}

void EscribirLedUp(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_OK, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_UP, PRENDER);
}

void EscribirLedOk(void) {
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_DOWN, APAGAR);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_OK, PRENDER);
	Chip_GPIO_SetPinState(LPC_GPIO, 0, LED_UP, APAGAR);
}
