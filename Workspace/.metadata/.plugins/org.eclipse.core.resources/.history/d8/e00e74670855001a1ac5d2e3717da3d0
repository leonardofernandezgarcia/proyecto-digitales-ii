/*
 * @brief Blinky example using sysTick
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2014
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "board.h"
#include <stdio.h>
#include "chip.h"
#include "board.h"
#include "string.h"
#include "stdio.h"
#include "arm_math.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define TICKRATE_HZ1 (10)	/* 10 ticks per second */
#define CANTIDAD_DE_MUESTRAS 2048
#define DOWNSAMPLE_RATE 2

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
static volatile uint8_t channelTC, dmaChannelNum;
uint16_t datosADC0[CANTIDAD_DE_MUESTRAS];
uint16_t datosADC1[CANTIDAD_DE_MUESTRAS];
uint16_t fft_tam = CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE;
//arm_cfft_radix4_instance_f32 ARM_CFFT_MODULE;
arm_rfft_fast_instance_f32 ARM_RFFT_MODULE;
arm_cfft_instance_f32 ARM_CFFT_MODULE;
char string_to_send [CANTIDAD_DE_MUESTRAS*6/DOWNSAMPLE_RATE] = "";
float32_t FFT_Input[CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE];
float32_t FFT_Output[CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE];
char auxiliarstring[DECIMALES];
char string_tmp [6];
uint16_t datosADCDownsampled[CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE];
uint16_t newData = 0;
int nro_muestra = 0;
float32_t pepe = 0;
/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void FFT_Inicializar(uint16_t tam); //Prototipo de la función FFT_Inicializar
void FFT_Calcular(float32_t* fft_input, float32_t * fft_output, uint16_t tam); //Prototipo de la función FFT_Calcular

/**
 * @brief	Handle interrupt from SysTick timer
 * @return	Nothing
 */
/**
 * @brief	main routine for systick example
 * @return	Function should not exit.
 */
int main(void)
{
	/* Generic Initialization */
	SystemCoreClockUpdate();
	Board_Init();


	/* LEDs toggle in interrupt handlers */
	while (1) {
		FFT_Inicializar(fft_tam);
		// creo escalon
		for (int i = 0; i<(CANTIDAD_DE_MUESTRAS); i++) {
			if (i <(CANTIDAD_DE_MUESTRAS/2)) {
				datosADC0[i]= 0;
			} else {
				datosADC0[i]= 1024;
			}
		}
		for(int a=0; a<(CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE); a++) {
			datosADCDownsampled[a] = 0;
			for (int b=0; b<DOWNSAMPLE_RATE; b++) {
				datosADCDownsampled[a] += datosADC0[DOWNSAMPLE_RATE*a + b];
			}
			datosADCDownsampled[a] = datosADCDownsampled[a] / DOWNSAMPLE_RATE;
		}
	}
	for(int i = 0; i <CANTIDAD_DE_MUESTRAS/DOWNSAMPLE_RATE; i++) {
		FFT_Input[i] = (float32_t) datosADCDownsampled[i];
		// Adaptacion de la lectura del ADC a volts
		FFT_Input[i] = (FFT_Input[i]*3.30f/4096)-(3.30f/2);
	}
	FFT_Calcular(FFT_Input, FFT_Output, fft_tam);
	pepe = FFT_Output[1];
	return 0;
}

void FFT_Inicializar(uint16_t tam) {
	//Inicializa el módulo RFFT, para realizar transformada FFT directas. Indica tamaaño y estructura
	arm_rfft_fast_init_f32(&ARM_RFFT_MODULE, tam);
	// Prueba con CFFT
	//arm_cfft_radix4_init_f32(&ARM_CFFT_MODULE, tam, 0, 0);
}

void FFT_Calcular(float32_t* fft_input, float32_t * fft_output, uint16_t tam) {
	//Extraído de libreria de DSP para Cortex-M3 de CMSIS
	/* Process the data through the RFFT/RIFFT module */
	uint8_t rfft = 0;
	arm_rfft_fast_f32(&ARM_RFFT_MODULE, fft_input, fft_output, rfft);
	/* Process the data through the Complex Magnitude Module for calculating the magnitude at each bin */
	//arm_cmplx_mag_f32(fft_input, fft_input, tam/2);
}
