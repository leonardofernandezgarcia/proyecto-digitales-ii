/**************************************************************
WinFilter version 0.8
http://www.winfilter.20m.com
akundert@hotmail.com

Filter type: Low Pass
Filter model: Butterworth
Filter order: 2
Sampling Frequency: 400 KHz
Cut Frequency: 20.000000 KHz
Coefficents Quantization: float

Z domain Zeros
z = -1.000000 + j 0.000000
z = -1.000000 + j 0.000000

Z domain Poles
z = 0.780509 + j -0.179324
z = 0.780509 + j 0.179324
***************************************************************/
#define Ntap 21

float fir(float NewSample) {
    float FIRCoef[Ntap] = { 
        -0.00201548336647219700,
        0.00024937455791915159,
        0.00411514867953746440,
        0.01016480612215028800,
        0.01909484683250016800,
        0.03170818527546587200,
        0.04889285294149270400,
        0.07158142870980123400,
        0.10068551873854978000,
        0.13699933428789904000,
        0.15704797444231294000,
        0.13699933428789904000,
        0.10068551873854978000,
        0.07158142870980123400,
        0.04889285294149270400,
        0.03170818527546587200,
        0.01909484683250016800,
        0.01016480612215028800,
        0.00411514867953746440,
        0.00024937455791915159,
        -0.00201548336647219700
    };

    static float x[Ntap]; //input samples
    float y=0;            //output sample
    int n;

    //shift the old samples
    for(n=Ntap-1; n>0; n--)
       x[n] = x[n-1];

    //Calculate the new output
    x[0] = NewSample;
    for(n=0; n<Ntap; n++)
        y += FIRCoef[n] * x[n];
    
    return y;
}
